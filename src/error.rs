#[derive(Debug)]
pub enum Error {
	Unknown,
	NotFound,
}

pub type Result<T> = std::result::Result<T, Error>;
