use std::collections::HashMap;
use std::hash::Hash;
use std::marker::PhantomData;

use crate::error::Error;
use crate::error::Result;
use crate::meta::Handler;
use crate::meta::Router;

type Cb<I> = fn(&I) -> Result<I>;

pub struct Named<C, B>
where
	C: Eq + Hash,
{
	list: HashMap<C, Box<Cb<B>>>,
	c: PhantomData<C>,
	b: PhantomData<B>,
}

impl<C, B> Named<C, B>
where
	C: Eq + Hash,
{
	pub fn new() -> Self {
		Self {
			list: HashMap::new(),
			c: PhantomData,
			b: PhantomData,
		}
	}
}

impl<C, B> Handler<(C, B), B> for Named<C, B>
where
	C: Eq + Hash,
{
	fn call(&self, (code, body): &(C, B)) -> Result<B> {
		if let Some(handler) = self.list.get(&code).cloned() {
			handler(&body)
		} else {
			Err(Error::NotFound)
		}
	}
}

impl<C, B> Router<(C, B), B> for Named<C, B>
where
	C: Eq + Hash,
{
	type Handler = (C, Cb<B>);
	fn set(&mut self, (code, handler): Self::Handler) -> Result<usize> {
		if let Some(_) = self.list.insert(code, Box::new(handler)) {
			Err(Error::Unknown)
		} else {
			Ok(self.list.len())
		}
	}
	fn unset(&mut self, _: usize) -> Result<Self::Handler> {
		unimplemented!()
	}
}
