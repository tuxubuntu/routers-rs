#![cfg(test)]

#[test]
fn and() {
	use crate::basic::*;
	use crate::msg::*;
	use crate::*;

	let mut a = Named::new();
	a.set(("summ", |x| Ok(x + x))).unwrap();
	a.set(("any", |x| Ok(x + x))).unwrap();

	let mut b = Named::new();
	b.set(("mult", |x| Ok(x * x))).unwrap();
	b.set(("any", |x| Ok(x * x))).unwrap();

	let c = Func::new(|_| Ok(0));

	let d = Pipe::new(
		And::new(And::new(a, b), c),
		Func::new(|((a, b), c)| Ok(a + b + c)),
	);

	assert!(d.call(&("summ", 12)).is_err());
	assert!(d.call(&("mult", 12)).is_err());
	assert!(d.call(&("other", 12)).is_err());
	assert_eq!(168, d.call(&("any", 12)).unwrap());
}

#[test]
fn join() {
	use crate::basic::*;
	use crate::msg::*;
	use crate::*;

	let mut a = Named::new();
	a.set((42, |x| Ok(x + x))).unwrap();

	let mut b = Named::new();
	b.set((73, |x| Ok(x * x))).unwrap();

	let c = Func::new(|_| Ok(0));

	let d = Join::new(Join::new(a, b), c);

	assert_eq!(24, d.call(&(42, 12)).unwrap());
	assert_eq!(144, d.call(&(73, 12)).unwrap());
	assert_eq!(0, d.call(&(32, 12)).unwrap());
}

#[test]
fn pipe() {
	use crate::basic::*;
	use crate::*;

	let mut a = Func::empty();
	a.set(|x| Ok(x + 1)).unwrap();
	let mut c = Func::empty();
	c.set(|x| Ok(x + x)).unwrap();
	let e = Pipe::new(a, c);
	assert_eq!(3 + 3, e.call(&2).unwrap());

	let mut a = Func::empty();
	a.set(|x| Ok(x + 1)).unwrap();
	let mut c = Func::empty();
	c.set(|x| Ok(x + x)).unwrap();
	let e = Pipe::new(c, a);
	assert_eq!(2 + 2 + 1, e.call(&2).unwrap());
}

#[test]
fn named() {
	use crate::msg::*;
	use crate::*;

	let mut a = Named::new();

	a.set((42, |x| Ok(x + x))).unwrap();
	a.set((73, |x| Ok(x * x))).unwrap();

	assert_eq!(24, a.call(&(42, 12)).unwrap());
	assert_eq!(144, a.call(&(73, 12)).unwrap());
}
