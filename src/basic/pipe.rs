use std::marker::PhantomData;

use crate::error::Result;
use crate::meta::Handler;

pub struct Pipe<A, B, I, X, O>
where
	A: Handler<I, X>,
	B: Handler<X, O>,
{
	a: A,
	b: B,
	i: PhantomData<I>,
	x: PhantomData<X>,
	o: PhantomData<O>,
}

impl<A, B, I, X, O> Pipe<A, B, I, X, O>
where
	A: Handler<I, X>,
	B: Handler<X, O>,
{
	pub fn new(a: A, b: B) -> Self {
		Self {
			a,
			b,
			i: PhantomData,
			x: PhantomData,
			o: PhantomData,
		}
	}
}

impl<A, B, I, X, O> Handler<I, O> for Pipe<A, B, I, X, O>
where
	A: Handler<I, X>,
	B: Handler<X, O>,
{
	fn call(&self, msg: &I) -> Result<O> {
		match self.a.call(msg) {
			Ok(res) => self.b.call(&res),
			Err(err) => Err(err),
		}
	}
}
