use std::marker::PhantomData;

use crate::error::Error;
use crate::error::Result;
use crate::meta::Handler;

pub struct Join<A, B, I, O>
where
	A: Handler<I, O>,
	B: Handler<I, O>,
{
	a: A,
	b: B,
	i: PhantomData<I>,
	o: PhantomData<O>,
}

impl<A, B, I, O> Join<A, B, I, O>
where
	A: Handler<I, O>,
	B: Handler<I, O>,
{
	pub fn new(a: A, b: B) -> Self {
		Self {
			a,
			b,
			i: PhantomData,
			o: PhantomData,
		}
	}
}

impl<A, B, I, O> Handler<I, O> for Join<A, B, I, O>
where
	A: Handler<I, O>,
	B: Handler<I, O>,
{
	fn call(&self, msg: &I) -> Result<O> {
		match self.a.call(msg) {
			Err(Error::NotFound) => self.b.call(msg),
			Ok(res) => Ok(res),
			Err(err) => Err(err),
		}
	}
}
