use crate::error::Result;
use crate::meta::Handler;
use crate::meta::Router;

type Cb<I, O> = fn(&I) -> Result<O>;

pub struct Func<I, O> {
	f: Box<Cb<I, O>>,
}

impl<I, O> Func<I, O> {
	pub fn new(cb: Cb<I, O>) -> Self {
		Self { f: Box::new(cb) }
	}
}

impl<I, O> Handler<I, O> for Func<I, O> {
	fn call(&self, msg: &I) -> Result<O> {
		(self.f)(msg)
	}
}

impl<I, O> Router<I, O> for Func<I, O> {
	type Handler = Cb<I, O>;
	fn set(&mut self, handler: Self::Handler) -> Result<usize> {
		self.f = Box::new(handler);
		Ok(0)
	}
	fn unset(&mut self, _: usize) -> Result<Self::Handler> {
		panic!("Can't unset body of function.");
	}
}

impl<I> Func<I, I>
where
	I: Clone,
{
	pub fn empty() -> Self {
		Self {
			f: Box::new(|x| Ok(x.clone())),
		}
	}
}

impl<I, O> From<Cb<I, O>> for Func<I, O> {
	fn from(cb: Cb<I, O>) -> Self {
		Func::new(cb)
	}
}
