use crate::error::Result;
use crate::meta::Handler;
use std::marker::PhantomData;

pub struct And<A, B, I, Oa, Ob>
where
	A: Handler<I, Oa>,
	B: Handler<I, Ob>,
{
	a: A,
	b: B,
	i: PhantomData<I>,
	oa: PhantomData<Oa>,
	ob: PhantomData<Ob>,
}

impl<A, B, I, Oa, Ob> And<A, B, I, Oa, Ob>
where
	A: Handler<I, Oa>,
	B: Handler<I, Ob>,
{
	pub fn new(a: A, b: B) -> Self {
		Self {
			a,
			b,
			i: PhantomData,
			oa: PhantomData,
			ob: PhantomData,
		}
	}
}

impl<A, B, I, Oa, Ob> Handler<I, (Oa, Ob)> for And<A, B, I, Oa, Ob>
where
	A: Handler<I, Oa>,
	B: Handler<I, Ob>,
{
	fn call(&self, msg: &I) -> Result<(Oa, Ob)> {
		Ok((self.a.call(msg)?, self.b.call(msg)?))
	}
}
