use crate::error::Result;

pub trait Handler<I, O> {
	fn call(&self, msg: &I) -> Result<O>;
}

pub trait Router<I, O>
where
	Self: Handler<I, O>,
{
	type Handler;
	fn set(&mut self, handler: Self::Handler) -> Result<usize>;
	fn unset(&mut self, hid: usize) -> Result<Self::Handler>;
}
